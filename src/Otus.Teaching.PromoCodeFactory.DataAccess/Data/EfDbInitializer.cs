﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            var pendingMigrations = _dataContext.Database.GetPendingMigrations().ToList();
            if (pendingMigrations.Any())
            {
                var migrator = _dataContext.Database.GetService<IMigrator>();
                foreach (var targetMigration in pendingMigrations)
                    migrator.Migrate(targetMigration);
            }
            else
            {
                _dataContext.Database.EnsureCreated();
            }
            
            if (!_dataContext.Employees.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();
            }

            if (!_dataContext.Preferences.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();
            }

            if (!_dataContext.Customers.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();
            }
        }
    }
}